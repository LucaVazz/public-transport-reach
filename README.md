# Public Transport Reach

Visualizes how far you can travel in a given time by using public transport and walking.


### Local Set-up
1. clone the repo
2. copy `auth\here_api_auth.example.js` to `auth\here_api_auth.js` and `auth\mapbox_auth.exmaple.js` to `auth\mapbox_auth.js` and fill in your credentials
3. serve the static files over any kind of web server

### Deployment Set-up
1. create a ConfigMap with name `public-transport-reach` and add two entries with keys `here_auth` and
    `mapbox_auth`, copy-pasting the values of the auth files from the `auth` folder
1. login to the cluster on your shell, than run `oc process -f deployment-template.yaml | oc apply -f -`
1. configure a web hook in the GitLab repo, copying the target URL from the settings of the `public-transport-reach` BuildConfig in OpenShift

### Acknowledgements
- [HERE Transit](https://developer.here.com/documentation/transit/topics/what-is.html)
    and [HERE Routing](https://developer.here.com/documentation/routing/topics/what-is.html)
    APIs are used for acquiring the data.
- [Mapbox GL JS](https://docs.mapbox.com/mapbox-gl-js/api/) is used for displaying the map
    and search results on it.
- [Vue](https://vuejs.org/v2/guide/) is used for combining UI and data logic.
- [Turf](https://turfjs.org/) is used for various operations on the processed geo data.
- [Meteocons](http://www.alessioatzeni.com/meteocons/) is used as icons for the time selection.
- [Fontelico Font](https://github.com/fontello/fontelico.font) 
    <sup>([CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/))</sup>
    is used as icons for error messages.


---


### Contributing
I'm open for all Forks, Feedback and Pull Requests.

### License
This project is licensed under the terms of the *GNU General Public License v3.0*. For further information, please look [here](http://choosealicense.com/licenses/gpl-3.0/) or [here<sup>(DE)</sup>](http://www.gnu.org/licenses/gpl-3.0.de.html).
