/**
 * Provides convenient abstractions over the functions for Mapbox.
 * see https://docs.mapbox.com/mapbox-gl-js/api/
 */


let map = {}


/**
 * Prepare the map
 * @param {string} accessToken the mapbox access token
 * @param {Function} readyFn function to be called once the map is done with the initial load
 * @param {Function} [clickFn] function to be called with the coords when a click on the map occurs
 * @return {void}
 */
export function setupMap(accessToken, readyFn, clickFn = (() => {})) {
    mapboxgl.accessToken = accessToken

    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/lucavazz/cjy0ufodf06fs1coao4k4fqmk',
        center: [8.67133, 50.11268],
        zoom: 12,
    })

    map.on('load', () => {
        readyFn()

        map.on('click', evt => {
            let coord = evt.lngLat
            clickFn(
                [coord.lng, coord.lat]
            )
        })
    })
}

/**
 * Replace the data for a layer with new points
 * @param {string} sourceName  identifier to access the data source
 * @param {FeatureCollection} data representation of data to show
 */
export function setMapLayerData(sourceName, data) {
    map
        .getSource(sourceName)
        .setData(data);
}

/**
 * Add an empty data source and matching layer to display data as points on the map.
 * @param  {string} sourceName identifier to access the data source later
 * @param  {string} color      CSS-Color Code for colouring the circles
 * @param  {number} [opacity]  opacity value for the point's fill, 1 is fully opaque
 * @return {void}
 */
export function preparePointsLayer(sourceName, color, opacity = 0.9) {
    map.addSource(sourceName, {
        "type": "geojson",
        "data": {
            "type": "FeatureCollection",
            "features": []
        }
    })
    map.addLayer({
        "id": `${sourceName}Layer`,
        "type": "circle",
        "source": sourceName,
        "paint": {
            "circle-color": color,
            "circle-opacity": opacity,
            // make circles larger as the user zooms from z12 to z22
            "circle-radius": {
                "base": 1,
                "stops": [
                    [9, 3],
                    [15, 10]
                ]
            },
            "circle-stroke-width": 0
        },
    })
}

/**
 * Add an empty data source and matching layer to display data as an area on the map.
 * @param  {string} sourceName    identifier to access the data source later
 * @param  {string} color         CSS-Color Code for colouring the area
 * @param  {number} [opacity]     opacity value for the area's fill, 1 is fully opaque
 * @return {void}
 */
export function prepareAreaLayer(sourceName, color, opacity = 0.7) {
    map.addSource(sourceName, {
        "type": "geojson",
        "data": {
            'type': 'Feature',
            'geometry': {
                'type': 'Polygon',
                'coordinates': [
                    []
                ]
            }
        }
    })
    map.addLayer({
        "id": `${sourceName}Layer`,
        "type": "fill",
        "source": sourceName,
        "paint": {
            "fill-color": color,
            "fill-opacity": opacity
        },
    }, getFirstMapSymbolLayerId())
}


/// HELPER FUNCTIONS:

function getFirstMapSymbolLayerId() {
    let layerList = map.getStyle().layers;
    let firstLayer = layerList.find(layer => layer.type === 'symbol')
    return firstLayer.id
}
