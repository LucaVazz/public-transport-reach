/**
 * Provides a neat interface to query the HERE API.
 * see
 * - https://developer.here.com/documentation/transit/topics/what-is.html
 * - https://developer.here.com/documentation/routing/topics/what-is.html
 */


/**
 * Helper method to call an HERE API endpoint.
 * @param {string} apiKey api access key for here maps
 * @param {string} subdomain top sub-domain to which the endpoint belongs
 * @param {string} endpoint where to sent the data to
 * @param {Object} params what to send as additional parameters to the endpoint
 * @return {Promise<Object>} The response, already parsed
 */
function fetchData(apiKey, subdomain, endpoint, params) {
    let targetUrl = new URL(endpoint, `https://${subdomain}.ls.hereapi.com`)
    targetUrl.searchParams.append('apiKey', apiKey)
    Object.keys(params).forEach(k => targetUrl.searchParams.append(k, params[k]))

    return fetch(new Request(targetUrl))
        .then(response => {
            if (!response.ok) {
                throw `HTTP error code ${response.status}`
            } else {
                return response.json() || {}
            }
        })
    ;
}


/// EXPORTED ENDPOINTS:

/**
 * Call the HERE API to get a list of stations which are reachable from a given center point.
 * @param {string} apiKey api access key for here maps
 * @param  {string} center Coordinate of the center point, concatenated with a comma
 * @param  {number} maxDur maximum duration of the journey itself, in minutes
 * @param  {string} time start time, in yy-MM-ddThh:mm:ss format
 * @return {Array<[number, number]>} list of coordinate-pairs for reachable stations
 */
export function getReachableTransitStations(apiKey, center, maxDur, time) {
    // see https://developer.here.com/documentation/transit/dev_guide/topics/example-reachability-area-within-specified-duration.html
	return fetchData(
        apiKey,
        'transit', 'v3/isochrone.json', {
            center: center, maxDur: maxDur, time: time
        })
		.then(responseBody => responseBody.Res.Isochrone)
        .then(isochrone => {
            if (!isochrone) {
                return []
            }
            
            return isochrone.IsoDest.map(entry => {
                let entryCoord = entry.Stn[0]
                return [entryCoord.x, entryCoord.y]
            })
        })
    ;
}

/**
 * Call the HERE API to get an area which is reachable from a given center point.
 * @param {string} apiKey api access key for here maps
 * @param  {string} center Coordinate of the center point, concatenated with a comma
 * @param  {number} maxDur maximum duration of the journey itself, in minutes
 * @return {Array<[number, number]>} list of coordinate-pairs for the outline of the reachable area
 */
export function getReachableWalkArea(apiKey, center, maxDur) {
    // https://developer.here.com/documentation/routing/dev_guide/topics/resource-calculate-isoline.html#resource-calculate-isoline
    return fetchData(
        apiKey,
        'isoline.route', 'routing/7.2/calculateisoline.json', {
            mode: 'fastest;pedestrian', rangetype: 'time', start: center, range: maxDur * 60
        }) // range is expected as seconds
        .then(responseBody => responseBody.response.isoline[0].component[0].shape)
        .then(coordList => {
            return coordList.map(coord => {
                let [_, y, x] = coord.match(/([0-9]+\.[0-9]+),([0-9]+\.[0-9]+)/)
                return [x, y]
            })
        })
    ;
}


/// HELPERS:

/**
 * Make coordinates from GeoJson compatible with the HERE API
 * @param  {Array<string>} geojsonCoord tuple of the coordinate
 * @return {string} coordinate formatted for the HERE API
 */
export function geojsonCoordToHereCoord(geojsonCoord) {
    let [x, y] = geojsonCoord
    return `${y},${x}`
}
