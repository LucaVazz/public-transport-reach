/**
 * Provides wrappers to mangle map data into easier to deal with form.
 */


/**
 * Reduce a set of GeoJson Points to a smaller set of Points.
 * Finds clusters of points near each other and reduces them to the center point of the clusters.
 * @param  {FeatureCollection<Point>} collection set of original points
 * @return {FeatureCollection<Point>} reduced set of points
 */
export function reducePoints(collection) {
    // scan for clusters:
    let clusteredCollection = turf.clustersDbscan(
        collection, 0.1 /* km */ , {
            'minPoints': 1
        }
    )

    // reduce clusters to center points:
    let centerPointsList = []
    turf.clusterEach(clusteredCollection, 'cluster', (cluster) => {
        centerPointsList.push(turf.centroid(cluster))
    })

    // return as FeatureCollection
    return turf.featureCollection(centerPointsList)
}

/**
 * Simplify a GeoJson FeatureCollection down to a List of Coordinates
 * @param  {FeatureCollection<Point>} collection the input to simplify
 * @return {Array<[number, number]>} list of tuples of the coordinates from the given points
 */
export function collectionToCoordList(collection) {
	let result = []
    turf.coordEach(collection, (coord) => result.push(coord))
    return result
}

/**
 * Transform a list of coordinate tuples to a GeoJson Feature Collection of Points.
 * @param  {Array<[number, number]>} coordList list of tuples for the coordinates of the Points
 * @return {FeatureCollection<Point>} equivalent FeatureCollection of Points
 */
export function coordListToFeatureCollection(coordList) {
    let pointsList = coordList.map(entry => turf.point(entry))
    return turf.featureCollection(pointsList)
}

/**
 * Transform a list of coordinate tuples to a GeoJson Polygon.
 * @param  {Array<[number, number]>} coordList list of tuples for the coordinates of the Polygon
 * @return {Feature<Polygon>} equivalent Polygon object
 */
export function coordListToPolygon(coordList) {
    let line = turf.lineString(coordList)
    let polygon = turf.polygonize(line).features[0]
    return turf.truncate(polygon, {mutate: true, coordinates: 2, precision: 5}) // simplify
}

/**
 * Speed up building the union of a list of GeoJson Polygons by splitting the work
 * @param  {Array<Feature<Polygon>>} polygonList list of Polygons to combine
 * @return {Feature<MultiPolygon|Polygon>} combined Polygon or MultiPolygon if the source Polygons
 *                                          aren't fully connected
 */
export async function fasterUnion(polygonList) {
	var polygonListShards = []
	while (polygonList.length > 0) {
	    polygonListShards.push(polygonList.splice(0, 10))
	    // 10 was chosen as the size by well-founded trail-and-error
	}

	return Promise
		.all(
			polygonListShards.map(partialPolygonList => turf.union(...partialPolygonList))
		)
		.then(
			results => turf.union(...results)
		)
	;
}
