/**
 * The main flow of the interaction.
 */

import {
    geojsonCoordToHereCoord, getReachableTransitStations, getReachableWalkArea
} from './lib/here_api.js'
import {
    coordListToFeatureCollection, reducePoints, collectionToCoordList, coordListToPolygon, 
    fasterUnion
} from './lib/map_data_transforms.js'
import {
    setupMap, prepareAreaLayer, preparePointsLayer, setMapLayerData
} from './lib/mapbox.js'

import { apiKey as hereApiKey } from './auth/here_api_auth.js'
import { accessToken as mapBoxAccessToken } from './auth/mapbox_auth.js'


var app = new Vue({
    el: '#appInput',
    data: {
        // dynamic input:
        homeCoord: null,
        transitTime: 35,
        walkingTime: 15,
        startTime: 13,
        startDay: 0,
        resultRawStations: null,
        isLoading: false,
        noResults: false,
        isError: false,
        // options
        words: {
            hey: [
                'Hey', 'Hi', 'Hello', 'Welcome', 'Hej', 'Greetings'
            ],
            wow: [
                'Wow', 'Whoa', 'Amazing', 'Impressive', 'Cool', 'Phsaw'
            ]
        },
        choices: {
            times: [
                {text: '8:00', value: 8, icon: 'A'}, {text: '13:00', value: 13, icon: 'B'}, 
                {text: '21:00', value: 21, icon: 'C'}, {text: '4:00', value: 4, icon: 'E'}
            ],
            days: [
                'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'
            ]
        },
        colors: {
            home: '#1B5E20',
            stations: '#4CAF50',
            reachableArea: '#66BB6A'
        }
    },

    created: function() {
        setupMap(
            mapBoxAccessToken,
            () => {
                preparePointsLayer('home', this.colors.home)
                preparePointsLayer('stationsRaw', this.colors.stations, 0.25)
                preparePointsLayer('stationsReduced', this.colors.stations)
                prepareAreaLayer('reachableArea', this.colors.reachableArea, 0.3, true)
            },
            (homeCoord) => {
                if (!app.resultRawStations) {
                    app.homeCoord = homeCoord
                }
            }
        )
    },
    computed: {
        homeCoordStr: function() {
            let coordStr = this.homeCoord
                .map(n => (+n).toFixed(6))
                .join(', ')
            ;
            return `(${coordStr})`
        },
        startDate: function() {
            // Wouldn't it be nice if JavaScript had a sane way to handle dates...?
            var date = new Date()
            date.setDate(date.getDate() + (
                + 7 // ensure its the next day, not a previous one
                + (app.startDay + 1 % 6) // to make 0 = Monday, not = Sunday
                - date.getDay()) // relative to the current day
                % 7 // bound to valid range
            )
            date.setUTCHours(app.startTime)
            date.setMinutes(0)
            date.setSeconds(0)
            return date.toISOString().slice(0, '0000-00-00T00:00:00'.length)
        },
        resultReducedStations: function() {
            if (this.resultRawStations === null) { return null }
            return reducePoints(this.resultRawStations)
        },
        resultStationCount: function() {
            if (this.resultRawStations === null) { return null }
            return this.resultReducedStations.features.length
        },
        resultAreaSize: function() {
            if (this.resultArea === null) { return null }
            let areaSizeSqm = turf.area(this.resultArea)
            let areaSizeSqkm = areaSizeSqm / 1000000
            return areaSizeSqkm.toFixed(2)
        }
    },
    asyncComputed: {
        resultArea: async function() {
            if (this.resultReducedStations === null) { return null }

            let coordList = collectionToCoordList(this.resultReducedStations)
            return await Promise
                .all(coordList.map(coord => getReachableWalkArea(
                    hereApiKey, geojsonCoordToHereCoord(coord), app.walkingTime
                )))
                .then(polygonsCoordLists => polygonsCoordLists.map(coordListToPolygon))
                .then(fasterUnion)
                .then(data => {
                    app.isLoading = false
                    return data
                })
                .catch(err => {
                    console.log('Error while fetching reachable area: ' + err)
                    app.isError = true
                })
            ;
        },
    },
    watch: { // show data on the map on changes:
        homeCoord: function(data) {
            data = turf.featureCollection([ turf.point(data) ])
            setMapLayerData('home', data)
        },
        resultRawStations: function(data) {
            setMapLayerData('stationsRaw', data || turf.featureCollection([]))
        },
        resultReducedStations: function(data) {
            setMapLayerData('stationsReduced', data || turf.featureCollection([]))
        },
        resultArea: function(data) {
            setMapLayerData('reachableArea', data || turf.featureCollection([]))
        }
    },

    methods: {
        onSearchClick: function() {
            const noResultsErr = 'NO_RESULTS'
            app.noResults = false
            app.isLoading = true

            getReachableTransitStations(
                hereApiKey, geojsonCoordToHereCoord(app.homeCoord), app.transitTime, app.startDate
            )
                .then(coordList => {
                    if (coordList.length == 0) {
                        app.noResults = true
                        app.isLoading = false
                        throw noResultsErr
                    } else {
                        return coordList
                    }
                })
                .then(coordListToFeatureCollection)
                .then(result => app.resultRawStations = result)
                .catch(err => {
                    if (err != noResultsErr) {
                        console.log('Error while fetching transit stations: ' + err)
                        app.isError = true
                    }
                })
            ;
        },
        onResetClick: function() {
            app.resultRawStations = null
        }
    },

    filters: { // helpers for the view:
        pick: function(arr) {
            return arr[Math.floor(Math.random() * arr.length)]
        }
    },
})
